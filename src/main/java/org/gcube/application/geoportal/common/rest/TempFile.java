package org.gcube.application.geoportal.common.rest;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class TempFile {

	private String id;
	private String filename;
	
}
