package org.gcube.application.geoportal.common.model.rest;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class DatabaseConnection {

    private String user;
    private String pwd;
    private String url;

}