package org.gcube.application.geoportal.common.faults;

public class JsonParseException extends Exception{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public JsonParseException() {
		super();
		// TODO Auto-generated constructor stub
	}

	public JsonParseException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
		// TODO Auto-generated constructor stub
	}

	public JsonParseException(String message, Throwable cause) {
		super(message, cause);
		// TODO Auto-generated constructor stub
	}

	public JsonParseException(String message) {
		super(message);
		// TODO Auto-generated constructor stub
	}

	public JsonParseException(Throwable cause) {
		super(cause);
		// TODO Auto-generated constructor stub
	}

	
	
}
