package org.gcube.application.geoportal.common.model.project;

import org.gcube.application.geoportal.common.model.BasicJSONObject;

import lombok.NoArgsConstructor;

@NoArgsConstructor
public class Project {

	
/**
 * Project{
 _id:
 profile_id:
 publication :{
   creation_time:
   creation_user:
   last_update_time:
   last_update_user:
   version :
   license :
   policy : 
 status : VALID,
    PUBLISHED,INVALID
 document : {.....}
 centroid : {
  x:
  y:
  z:}
 }

 * 
 */
	
	private String _id;
	private String profile_id;
	private Status status;
	private Object document;
	private Centroid centroid;
	private PublicationDetails publication;
	
	private String json;
}
