package org.gcube.application.geoportal.common.model;

import java.io.IOException;

import org.gcube.application.geoportal.common.model.legacy.Concessione;
import org.gcube.application.geoportal.common.model.profile.Profile;
import org.gcube.application.geoportal.common.utils.Files;
import org.junit.Assert;
import org.junit.Test;

import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;

public class SerializationTest {

	
	private static ObjectMapper mapper = new ObjectMapper();

	static {
		mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES,false);
		mapper.configure(SerializationFeature.FAIL_ON_EMPTY_BEANS,false);
		mapper.configure(SerializationFeature.WRITE_EMPTY_JSON_ARRAYS, false);
		mapper.setSerializationInclusion(Include.NON_NULL);
		//		mapper.configure(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS, false);
		mapper.registerModule(new JavaTimeModule());
	}

	
	@Test
	public void readProfile() throws JsonProcessingException, IOException {
		
		Profile concessione=mapper.readerFor(Profile.class).readValue(
				Files.getFileFromResources("ProfileConcessioni.json"));
		System.out.println("Profile is "+mapper.writeValueAsString(concessione));
		
		Assert.assertTrue(concessione.getFields().size()>0);
		
	}
	
	@Test
	public void readConcessione() throws JsonProcessingException, IOException {		
		Concessione concessione=mapper.readerFor(Concessione.class).readValue(
				Files.getFileFromResources("Concessione.json"));
		System.out.println("Concessione is "+concessione.toString());		
		
	}

	@Test
	public void generic() throws JsonProcessingException, IOException {
		Concessione conc=TestModel.prepareConcessione();
		conc.validate();
		full(conc);
	}
	
	
	public void full(Object obj) throws JsonProcessingException, IOException {
		String asString=mapper.writeValueAsString(obj);
		Object other=mapper.readerFor(obj.getClass()).readValue(asString);
		
	}
}
