package org.gcube.application.geoportal.common.model.profile;

import java.util.List;

import javax.xml.bind.annotation.XmlRootElement;

import lombok.Getter;
import lombok.NoArgsConstructor;

@Getter
@NoArgsConstructor
@XmlRootElement
public class Profile{

	private String name;
	private String id;
	private List<Field> fields;
	
	private List<DefaultCompiler> defaultCompilers;
	private List<Validator> validators;
	private IsoMapper isoMapper;
	private List<FieldMapping> centroidsMapping;
	private List<IndexDefinition> indexes;
}
