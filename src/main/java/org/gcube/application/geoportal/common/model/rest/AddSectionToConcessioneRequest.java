package org.gcube.application.geoportal.common.model.rest;

import java.util.List;

import javax.xml.bind.annotation.XmlRootElement;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.gcube.application.geoportal.common.rest.TempFile;

@XmlRootElement
@Data
@AllArgsConstructor
@NoArgsConstructor
public class AddSectionToConcessioneRequest {

	private String destinationPath;
	private List<TempFile> streams;
	
}
