package org.gcube.application.geoportal.common.model;

import static org.junit.Assert.assertEquals;

import java.util.UUID;

import org.gcube.application.geoportal.common.model.legacy.Concessione;
import org.gcube.application.geoportal.common.model.legacy.Concessione.Paths;
import org.gcube.application.geoportal.common.model.legacy.LayerConcessione;
import org.gcube.application.geoportal.common.model.legacy.RelazioneScavo;
import org.gcube.application.geoportal.common.model.legacy.UploadedImage;
import org.junit.Test;


public class PathsTest {

	
	@Test
	public void embeddedPaths() {
		Concessione c=TestModel.prepareConcessione();
		c=TestModel.setIds(c);
		
		
		
		LayerConcessione p=(LayerConcessione) c.getContentByPath(Paths.POSIZIONAMENTO);
		assertEquals(c.getPosizionamentoScavo(), p);
		
		RelazioneScavo rel=(RelazioneScavo) c.getContentByPath(Paths.RELAZIONE);
		assertEquals(c.getRelazioneScavo(), rel);
		
		for(int i=0;i<c.getPianteFineScavo().size();i++) {
			LayerConcessione l=(LayerConcessione) c.getContentByPath(Paths.piantaByIndex(i));
			assertEquals(c.getPianteFineScavo().get(i),l);
		}
		
		for(int i=0;i<c.getImmaginiRappresentative().size();i++) {
			UploadedImage l=(UploadedImage) c.getContentByPath(Paths.imgByIndex(i));
			assertEquals(c.getImmaginiRappresentative().get(i),l);
		}
		
		
		for(int i=0;i<c.getPianteFineScavo().size();i++) {
			LayerConcessione layer=c.getPianteFineScavo().get(i);
			LayerConcessione l=(LayerConcessione) c.getContentByPath(Paths.piantaById(layer.getMongo_id()));
			assertEquals(layer,l);
		}
		
		
		
		for(int i=0;i<c.getImmaginiRappresentative().size();i++) {
			UploadedImage layer=c.getImmaginiRappresentative().get(i);
			UploadedImage l=(UploadedImage) c.getContentByPath(Paths.imgById(layer.getMongo_id()));
			assertEquals(layer,l);
		}
		
		
		System.out.println();
		System.out.println();
		
		
		
		
	}

	
	
}
