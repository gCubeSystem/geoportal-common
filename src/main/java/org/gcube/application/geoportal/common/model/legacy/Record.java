package org.gcube.application.geoportal.common.model.legacy;

import java.time.LocalDateTime;

import org.gcube.application.geoportal.common.model.legacy.report.ConstraintCheck;
import org.gcube.application.geoportal.common.model.legacy.report.ValidationReport;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
@EqualsAndHashCode

public abstract class Record {

	private String mongo_id;
	
	//Generic Info
	private long id;	
	
	private RecordType recordType;
	private String version;
	private String licenzaID;
	
	private AccessPolicy policy;
	//Nome del progetto	
	private String nome;
	
	
	//Storage Info
	private String folderId;
	
	//Accounting
	private LocalDateTime lastUpdateTime;
	private String lastUpdateUser;
	private LocalDateTime creationTime;
	private String creationUser;
	

	
	private ValidationReport report;
	
	public ValidationReport validate() {
		
		setDefaults();
		ValidationReport validator=new ValidationReport("Core metadata");
		validator.checkMandatory(getRecordType(), "Record Type");
		
		validator.checkMandatory(getNome(), "Nome");
		setReport(validator);
		
		return getReport(); 
	}

	public void setDefaults() {
		LocalDateTime now=LocalDateTime.now();
		
		setVersion(ConstraintCheck.defaultFor(getVersion(),"1.0.0").evaluate());
		setPolicy(ConstraintCheck.defaultFor(getPolicy(), AccessPolicy.OPEN).evaluate());

		setLastUpdateTime(ConstraintCheck.defaultFor(getLastUpdateTime(),now).evaluate());
		setCreationTime(ConstraintCheck.defaultFor(getCreationTime(),now).evaluate());
		setLicenzaID(ConstraintCheck.defaultFor(getLicenzaID(),"CC-BY").evaluate());
	}
	
	
	public abstract AssociatedContent getContentByPath(String path);
	public abstract void setAtPath(AssociatedContent toSet,String path);
	
}
