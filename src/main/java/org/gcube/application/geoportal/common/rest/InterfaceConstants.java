package org.gcube.application.geoportal.common.rest;

public class InterfaceConstants {

	public static final String NAMESPACE="http://gcube-system.org/namespaces/data/sdi-service";

	public static final String APPLICATION_BASE_PATH="geoportal-service";
	public static final String APPLICATION_PATH="/srv";
	public static final String SERVICE_CLASS="Application";
	public static final String SERVICE_NAME="GeoPortal";

	public static final class Methods{
		public static final String PROFILES="profiles";
		public static final String SECTIONS="sections";
		public static final String PROJECTS="projects";
		
		public static final String CONCESSIONI="concessioni";
		public static final String MONGO_CONCESSIONI="mongo-concessioni";
		
		
		public static final String PUBLISH_PATH="publish";
		public static final String REGISTER_FILES_PATH="registerFiles";
		public static final String DELETE_FILES_PATH="deleteFiles";
		public static final String CONFIGURATION_PATH="configuration";
		public static final String SEARCH_PATH="search";
		public static final String QUERY_PATH="query";
		
	}

	public static final class Parameters{
		public static final String PROJECT_ID="project_id";
		public static final String SECTION_ID="section_id";
		public static final String PROFILE_ID="profile_id";

//		//INVESTIGATE CAPABILITIES
//		public static final String ORDER_BY="order_by";
//		public static final String LIMIT="limit";
//		public static final String OFFSET="offset";
		public static final String FORCE="force";

	}


}
