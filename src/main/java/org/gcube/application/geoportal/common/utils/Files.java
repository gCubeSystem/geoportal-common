package org.gcube.application.geoportal.common.utils;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.nio.charset.Charset;
import java.nio.file.Paths;

public class Files {

	public static File getFileFromResources(String fileName) {

        ClassLoader classLoader =Files.class.getClassLoader();

        URL resource = classLoader.getResource(fileName);
        if (resource == null) {
            throw new IllegalArgumentException("file is not found!");
        } else {
            return new File(resource.getFile());
        }

    }	


	public static String readFileAsString(String path, Charset encoding) 
			  throws IOException 
	{
	  byte[] encoded = java.nio.file.Files.readAllBytes(Paths.get(path));
	  return new String(encoded, encoding);
	}
	
	
	public static String getName(String path) {
		
		return path.substring((path.contains(File.separator)?path.lastIndexOf(File.separator)+1:0)
				,(path.contains(".")?path.lastIndexOf("."):path.length()));
		
	}
	
	public static String fixFilename(String toFix) {
		if(toFix.contains(".")) {
			String prefix=toFix.substring(toFix.lastIndexOf("."));
			toFix=toFix.substring(0,toFix.lastIndexOf("."));
			return toFix.toLowerCase().replaceAll("[\\*\\+\\/\\\\ \\[\\]\\(\\)\\.\\\"\\:\\;\\|]","_")+prefix;
		}
		return toFix.toLowerCase().replaceAll("[\\*\\+\\/\\\\ \\[\\]\\(\\)\\.\\\"\\:\\;\\|]","_");
	}
}
