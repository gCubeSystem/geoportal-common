package org.gcube.application.geoportal.common.model.legacy;

import java.util.List;

import org.gcube.application.geoportal.common.utils.CollectionsUtils;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString(callSuper=true)

public class UploadedImage extends AssociatedContent {

	
	private String didascalia;
	private String format;
	
	private List<String> responsabili;
	private List<String> soggetto;

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((didascalia == null) ? 0 : didascalia.hashCode());
		result = prime * result + ((format == null) ? 0 : format.hashCode());
//		result = prime * result + ((responsabili == null) ? 0 : responsabili.hashCode());
		result = prime * result + CollectionsUtils.hashCode(responsabili);
//		result = prime * result + ((soggetto == null) ? 0 : soggetto.hashCode());
		result = prime * result + CollectionsUtils.hashCode(soggetto);
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		UploadedImage other = (UploadedImage) obj;
		if (didascalia == null) {
			if (other.didascalia != null)
				return false;
		} else if (!didascalia.equals(other.didascalia))
			return false;
		if (format == null) {
			if (other.format != null)
				return false;
		} else if (!format.equals(other.format))
			return false;
//		if (responsabili == null) {
//			if (other.responsabili != null)
//				return false;
//		} else if (!responsabili.equals(other.responsabili))
//			return false;
		if(!CollectionsUtils.equalsCollections(responsabili, other.responsabili)) return false;
		
//		if (soggetto == null) {
//			if (other.soggetto != null)
//				return false;
//		} else if (!soggetto.equals(other.soggetto))
//			return false;
		if(!CollectionsUtils.equalsCollections(soggetto, other.soggetto)) return false;
		
		return true;
	}

}
