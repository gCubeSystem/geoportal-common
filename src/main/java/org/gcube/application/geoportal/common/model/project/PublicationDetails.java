package org.gcube.application.geoportal.common.model.project;

import java.time.LocalDateTime;

import lombok.Data;

@Data
public class PublicationDetails {

	public static enum Policy{
		OPEN,RESTRICTED,EMBARGOED;
	}
	
	private LocalDateTime creation_time;
	private String creation_user;
	private LocalDateTime last_update_time;
	private String last_update_user;
	private String version;
	private String license;
	private String policy;
		   
}
