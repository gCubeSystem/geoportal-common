package org.gcube.application.geoportal.common.model.rest;

import lombok.Data;

@Data
public class Configuration {

    // Index (postgis + layer) Configuration
    public PostgisIndexDescriptor index;



    // Mongo DB Configuration
    // TBD
}
