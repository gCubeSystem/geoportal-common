package org.gcube.application.geoportal.common.model.project;

public enum StatusPhase {

	DRAFT,
	UNDER_VALIDATION,
	INVALID,
	VALID,
	UNDER_PUBLICATION,
	PUBLICATION_ERROR,
	PUBLISHED
	
}
