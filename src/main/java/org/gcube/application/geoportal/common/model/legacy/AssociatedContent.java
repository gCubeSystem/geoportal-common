package org.gcube.application.geoportal.common.model.legacy;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElements;

import org.gcube.application.geoportal.common.model.legacy.report.ValidationReport;
import org.gcube.application.geoportal.common.utils.CollectionsUtils;

import lombok.Getter;
import lombok.Setter;


@Getter
@Setter

public abstract class AssociatedContent {
	
	private String mongo_id;
	private long id;
	
	private AccessPolicy policy;
	private String licenseID;
	private String titolo;
	private LocalDateTime creationTime;
	
	
	public ValidationReport validateForInsertion() {
		ValidationReport toReturn=new ValidationReport("Associated Content");
		toReturn.checkMandatory(policy, "Politica di accesso");
		toReturn.checkMandatory(licenseID, "Licenza");
		toReturn.checkMandatory(titolo,"Titolo");
		toReturn.checkMandatory(creationTime, "Creation time");
		return toReturn;
	}
	
	

	@XmlElements({
        @XmlElement(type=GeoServerContent.class),
        @XmlElement(type=WorkspaceContent.class),
    })

	private List<PersistedContent> actualContent=new ArrayList<>();



	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
//		result = prime * result + ((actualContent == null) ? 0 : actualContent.hashCode());
		result = prime * result + CollectionsUtils.hashCode(actualContent);
		
		result = prime * result + ((creationTime == null) ? 0 : creationTime.hashCode());
		result = prime * result + (int) (id ^ (id >>> 32));
		result = prime * result + ((licenseID == null) ? 0 : licenseID.hashCode());
		result = prime * result + ((policy == null) ? 0 : policy.hashCode());
		result = prime * result + ((titolo == null) ? 0 : titolo.hashCode());
		return result;
	}



	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		AssociatedContent other = (AssociatedContent) obj;
//		if (actualContent == null) {
//			if (other.actualContent != null)
//				return false;
//		} else if (!actualContent.equals(other.actualContent))
//			return false;
		if(!CollectionsUtils.equalsCollections(actualContent, other.actualContent)) return false;
		
		if (creationTime == null) {
			if (other.creationTime != null)
				return false;
		} else if (!creationTime.equals(other.creationTime))
			return false;
		if (id != other.id)
			return false;
		if (licenseID == null) {
			if (other.licenseID != null)
				return false;
		} else if (!licenseID.equals(other.licenseID))
			return false;
		if (policy != other.policy)
			return false;
		if (titolo == null) {
			if (other.titolo != null)
				return false;
		} else if (!titolo.equals(other.titolo))
			return false;
		return true;
	}



	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("AssociatedContent [id=");
		builder.append(id);
		builder.append(", policy=");
		builder.append(policy);
		builder.append(", licenseID=");
		builder.append(licenseID);
		builder.append(", titolo=");
		builder.append(titolo);
		builder.append(", creationTime=");
		builder.append(creationTime);
		
		builder.append(", actualContent=");
		builder.append(actualContent);
		builder.append("]");
		return builder.toString();
	}



	
	
}
