This project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

# Changelog for org.gcube.application.geoportal-common

# [v1.0.6-SNAPSHOT] - 2021-08-3
Forced Deletion
Search & query
Clean Fileset
Interfaces return iterator instead of iterable
Name in WorkspaceContent (https://support.d4science.org/issues/22032)


# [v1.0.5] - 2020-12-9
Mongo Id in record
Mongo Concessioni interface 
Added Files.fixFileNAme
ValidationReport as field
Updated Model (#20357)


# [v1.0.4-SNAPSHOT] - 2020-12-9
Projects Rest Interface
TempFile support

# [v1.0.3] - 2020-12-4
Project model update


# [v1.0.2-SNAPSHOT] - 2020-12-4
Model update

## [v1.0.1] - 2020-11-11

Model update

## [v1.0.0] - 2020-11-11

First release