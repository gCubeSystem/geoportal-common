package org.gcube.application.geoportal.common.utils;

import java.io.FileNotFoundException;
import java.io.InputStream;
import java.util.UUID;

import org.gcube.application.geoportal.common.rest.InterfaceConstants;
import org.gcube.application.geoportal.common.rest.TempFile;
import org.gcube.contentmanagement.blobstorage.service.IClient;
import org.gcube.contentmanagement.blobstorage.transport.backend.RemoteBackendException;
import org.gcube.contentmanager.storageclient.wrapper.AccessType;
import org.gcube.contentmanager.storageclient.wrapper.MemoryType;
import org.gcube.contentmanager.storageclient.wrapper.StorageClient;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class StorageUtils {

	public static final IClient getClient(){
		return new StorageClient(InterfaceConstants.SERVICE_CLASS, InterfaceConstants.SERVICE_NAME, ContextUtils.getCurrentCaller(), AccessType.SHARED, MemoryType.VOLATILE).getClient();
	}
	
	private IClient client;
	public StorageUtils() {
		client=getClient();
	}
	
	//return Id
	public TempFile putOntoStorage(InputStream source,String filename) throws RemoteBackendException, FileNotFoundException{		
		log.debug("Uploading source "+filename);
		String id=client.put(true).LFile(source).RFile(getUniqueString());
		return new TempFile(id,filename);
	}
	
	public static final boolean checkStorageId(String id){
		return getClient().getHttpUrl().RFile(id)!=null;
	}
	
	public static final String getUrlById(String id){
		IClient client=getClient();
		log.debug("Id is "+id);
		return client.getHttpUrl().RFile(id);
	}
	
	public static final void removeById(String id){
		IClient client=getClient();
		client.remove().RFile(id);
	}

	
	public static final String getUniqueString(){
		return UUID.randomUUID().toString();
	}
}
