package org.gcube.application.geoportal.common.rest;

import org.gcube.application.geoportal.common.model.project.Project;

import java.util.Iterator;

public interface ProjectsI {

	
	public Iterator<Project> getAll() throws Exception;
	public Iterator<Project> getByProfile(String profileId) throws Exception;
	public Project getById(String profileId,String id) throws Exception;
	public Iterator<Project> getByFilter(String filter)throws Exception;
	public Iterator<Project> getByFilter(String filter, String profileId)throws Exception;
	public Project registrNew(String profileId, String jsonDocument)throws Exception;
	public Project update(String profileId, String projectId,String jsonDocument) throws Exception;
	public void deleteById(String profileId, String projectId)throws Exception;
	public void deleteById(String profileId, String projectId, Boolean force)throws Exception;
}
