package org.gcube.application.geoportal.common.model.legacy;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString(callSuper=true)
public class AbstractRelazione extends AssociatedContent{

    private String abstractIta;
    private String abstractEng;


}
