package org.gcube.application.geoportal.common.rest;

import org.gcube.application.geoportal.common.model.legacy.Concessione;
import org.gcube.application.geoportal.common.model.rest.AddSectionToConcessioneRequest;
import org.gcube.application.geoportal.common.model.rest.Configuration;
import org.gcube.application.geoportal.common.model.rest.QueryRequest;

import java.util.Iterator;

public interface MongoConcessioni {

	public Concessione createNew(Concessione c) throws Exception;
	public void deleteById(String id) throws Exception;
	public void deleteById(String id,Boolean force) throws Exception;
	public Concessione getById(String id) throws Exception;
	public Iterator<Concessione> getList()throws Exception;
	public Concessione publish(String id) throws Exception;
	public Concessione registerFileSet(String id, AddSectionToConcessioneRequest request) throws Exception;
	public Concessione cleanFileSet(String id, String path) throws Exception;
	public Concessione update(String id, String jsonUpdate) throws Exception;
	public Concessione replace(Concessione replacement) throws Exception;

	public void unPublish(String id)throws Exception;


	public Configuration getCurrentConfiguration()throws Exception;
	public Iterator<Concessione> search(String filter)throws Exception;
	public Iterator<Concessione> query(QueryRequest request) throws Exception;
	public String queryForJSON(QueryRequest request) throws Exception;

	public <T> Iterator<T> queryForType(QueryRequest request,Class<T> clazz) throws Exception;
}
